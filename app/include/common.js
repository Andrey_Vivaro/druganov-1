$(function() {

// Mobile menu
$(".nav").click(function(e) {
	e.preventDefault();
	$(this).toggleClass("nav-expanded");
});


	// slick carousel
	$('.rev_slider-js').slick({
		dots: true,
		arrows: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		focusOnSelect: true,
		infinite: true,
		centerMode: true,
		responsive: [
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});

$('.slick-cloned a').removeAttr('data-fancybox');

});